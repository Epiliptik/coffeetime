# coffeetime

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### TODO
Add weather api
mode SFW and NSFW

### Conf ovh
inside .htaccess

RewriteEngine on
RewriteCond %{SERVER_PORT} 80
RewriteRule ^(.*)$ https://coffeetime.ovh/$1 [R,L]

RewriteEngine on
RewriteCond %{SERVER_PORT} 80 [OR]
RewriteCond %{HTTP_HOST} ^coffeetime\.ovh$ [NC]
RewriteRule ^(.*) https://coffeetime.ovh/$1 [QSA,L,R=301]
Header set Strict-Transport-Security "max-age=15811200" env=HTTPS